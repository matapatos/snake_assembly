# snake_assembly

This project contains gas Assembly code of a simple Snake game for **Linux 64-bit** architecture.

This game is composed by three main views:

- Starting view;
- Game view;
- Game over view.

See more details in the figure that follows:

![Game overview](images/readme/game.png)

## Core functionalities ##

- Game mechanics;
- Handling IO keyboard events;
- Reading and writing to files (e.g. reading and saving new best score);
- Rendering graphics (e.g. player, seed and text);
- Playing sounds (e.g. background game music as well as game over sound).

## External C libraries ##

Some external C libraries have also been used for developing this game, like:

- [Simple DirectMedia Layer 2 (SDL2)](https://www.libsdl.org/) -> For graphics.
- [SDL_ttf 2.0](https://www.libsdl.org/projects/SDL_ttf/) -> For text rendering.
- [Simple-SDL2-Audio](https://github.com/jakebesworth/Simple-SDL2-Audio) -> For playing sounds.

## Compiling and linking ##

**Note:** Before continuing, make sure that you have GCC installed as well as *SDL* and *SDL_ttf 2.0*, previously mentioned in *External C libraries* section.

For compiling and linking the game you can run the following Bash script:

```
./compile_and_link.sh
```

In the end, a *game* file should have been created in the game directory.

## Running game ##

Before running the game, you need to create the *game* executable file by following the previous step, *Compiling and linking*.
Afterwards, you just need to execute the *game* file as follows:

```
./game
```