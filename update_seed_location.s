.section .text
.type update_seed_location, @function
.globl update_seed_location
/**
Check if Player have colided with Seed, and if yes it updates the Player
properties (e.g. score and num parts), as well as sets a new location for
the Seed. Otherwise it returns.

NOTE: It does not require any input or output. It automatically uses the
	  Player and Seed properties.

Pseudo-code:
	void update_seed_location() {
		snake_head = Player.parts[0];
		if(snake_head.x == Seed.x && snake_head.y == Seed.y) {
			Player.score += 1;
			if(Player.num_parts < MAX_PLAYER_PARTS) {
				Player.parts[Player.num_parts].width = RECT_SIZE;
				Player.parts[Player.num_parts].height = RECT_SIZE;
				Player.num_parts += 1;
				// The X and Y position is automatically set up in the 'update_player_position' function.
			}
		}
	}
**/
update_seed_location:
	movl 	-968(%rbp), %eax
    cmpl 	%eax, -996(%rbp)
	jne 	.return
	movl 	-964(%rbp), %eax
    cmpl 	%eax, -992(%rbp)
	jne 	.return
	call 	randomly_init_seed
	.update_player_properties:
		incl 	-976(%rbp)  # Increment Player points
		# Check if num of parts reached limit otherwise, increment it
		movl 	PLAYER_MAX_PARTS, %eax
		cmpl 	%eax, -972(%rbp)
		jge 	.return
		incl 	-972(%rbp)
	.return:
		ret

.section .text
.type randomly_init_seed, @function
.globl randomly_init_seed
/**
Automatically set up a new position to Seed. It uses the auxiliary
function 'inclusive_random_range'.

NOTE: It does not require any input and it does not have any output.
	  It automatically sets the X and Y position to the Seed properties.
**/
randomly_init_seed:
    movl 	GAME_BORDERS, %esi
    movl 	$0, %edi
    call 	inclusive_random_range  # Random X
	mull 	RECT_SIZE
    movl 	%eax, -996(%rbp)
    movl 	GAME_BORDERS+4, %esi
    movl 	$0, %edi
    call 	inclusive_random_range  # Random Y
	mull 	RECT_SIZE
    movl 	%eax, -992(%rbp)
	movl 	RECT_SIZE, %eax
	movl 	%eax, -988(%rbp)  # Width
	movl	RECT_SIZE+4, %eax
	movl 	%eax, -984(%rbp)  # Height
    ret
