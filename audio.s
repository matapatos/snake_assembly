	.file	"audio.c"
	.local	gDevice
	.comm	gDevice,8,8
	.local	gSoundCount
	.comm	gSoundCount,4,4
	.text
	.globl	playSound
	.type	playSound, @function
playSound:
.LFB508:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movl	%esi, -12(%rbp)
	movl	-12(%rbp), %edx
	movq	-8(%rbp), %rax
	movl	%edx, %ecx
	movl	$0, %edx
	movl	$0, %esi
	movq	%rax, %rdi
	call	playAudio
	nop
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE508:
	.size	playSound, .-playSound
	.globl	playMusic
	.type	playMusic, @function
playMusic:
.LFB509:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movl	%esi, -12(%rbp)
	movl	-12(%rbp), %edx
	movq	-8(%rbp), %rax
	movl	%edx, %ecx
	movl	$1, %edx
	movl	$0, %esi
	movq	%rax, %rdi
	call	playAudio
	nop
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE509:
	.size	playMusic, .-playMusic
	.globl	playSoundFromMemory
	.type	playSoundFromMemory, @function
playSoundFromMemory:
.LFB510:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movl	%esi, -12(%rbp)
	movl	-12(%rbp), %edx
	movq	-8(%rbp), %rax
	movl	%edx, %ecx
	movl	$0, %edx
	movq	%rax, %rsi
	movl	$0, %edi
	call	playAudio
	nop
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE510:
	.size	playSoundFromMemory, .-playSoundFromMemory
	.globl	playMusicFromMemory
	.type	playMusicFromMemory, @function
playMusicFromMemory:
.LFB511:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movl	%esi, -12(%rbp)
	movl	-12(%rbp), %edx
	movq	-8(%rbp), %rax
	movl	%edx, %ecx
	movl	$1, %edx
	movq	%rax, %rsi
	movl	$0, %edi
	call	playAudio
	nop
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE511:
	.size	playMusicFromMemory, .-playMusicFromMemory
	.section	.rodata
.LC0:
	.string	"external_c_libraries/audio.c"
	.align 8
.LC1:
	.string	"[%s: %d]Fatal Error: Memory c-allocation error\n"
	.align 8
.LC2:
	.string	"[%s: %d]Error: SDL_INIT_AUDIO not initialized\n"
	.align 8
.LC3:
	.string	"[%s: %d]Error: Memory allocation error\n"
	.align 8
.LC4:
	.string	"[%s: %d]Warning: failed to open audio device: %s\n"
	.text
	.globl	initAudio
	.type	initAudio, @function
initAudio:
.LFB512:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -24
	movl	$48, %esi
	movl	$1, %edi
	call	calloc
	movq	%rax, gDevice(%rip)
	movl	$0, gSoundCount(%rip)
	movq	gDevice(%rip), %rax
	testq	%rax, %rax
	jne	.L6
	movq	stderr(%rip), %rax
	movl	$145, %ecx
	movl	$.LC0, %edx
	movl	$.LC1, %esi
	movq	%rax, %rdi
	movl	$0, %eax
	call	fprintf
	jmp	.L5
.L6:
	movq	gDevice(%rip), %rax
	movb	$0, 40(%rax)
	movl	$16, %edi
	call	SDL_WasInit
	andl	$16, %eax
	testl	%eax, %eax
	jne	.L8
	movq	stderr(%rip), %rax
	movl	$153, %ecx
	movl	$.LC0, %edx
	movl	$.LC2, %esi
	movq	%rax, %rdi
	movl	$0, %eax
	call	fprintf
	jmp	.L5
.L8:
	movq	gDevice(%rip), %rax
	addq	$8, %rax
	movl	$32, %edx
	movl	$0, %esi
	movq	%rax, %rdi
	call	SDL_memset
	movq	gDevice(%rip), %rax
	movl	$48000, 8(%rax)
	movq	gDevice(%rip), %rax
	movw	$-32752, 12(%rax)
	movq	gDevice(%rip), %rax
	movb	$2, 14(%rax)
	movq	gDevice(%rip), %rax
	movw	$4096, 16(%rax)
	movq	gDevice(%rip), %rax
	movq	$audioCallback, 24(%rax)
	movq	gDevice(%rip), %rbx
	movl	$72, %esi
	movl	$1, %edi
	call	calloc
	movq	%rax, 32(%rbx)
	movq	gDevice(%rip), %rax
	movq	32(%rax), %rax
	movq	%rax, -24(%rbp)
	cmpq	$0, -24(%rbp)
	jne	.L9
	movq	stderr(%rip), %rax
	movl	$170, %ecx
	movl	$.LC0, %edx
	movl	$.LC3, %esi
	movq	%rax, %rdi
	movl	$0, %eax
	call	fprintf
	jmp	.L5
.L9:
	movq	-24(%rbp), %rax
	movq	$0, 16(%rax)
	movq	-24(%rbp), %rax
	movq	$0, 64(%rax)
	movq	gDevice(%rip), %rbx
	movq	gDevice(%rip), %rax
	addq	$8, %rax
	movl	$7, %r8d
	movl	$0, %ecx
	movq	%rax, %rdx
	movl	$0, %esi
	movl	$0, %edi
	call	SDL_OpenAudioDevice
	movl	%eax, (%rbx)
	movl	(%rbx), %eax
	testl	%eax, %eax
	jne	.L10
	call	SDL_GetError
	movq	%rax, %rdx
	movq	stderr(%rip), %rax
	movq	%rdx, %r8
	movl	$180, %ecx
	movl	$.LC0, %edx
	movl	$.LC4, %esi
	movq	%rax, %rdi
	movl	$0, %eax
	call	fprintf
	jmp	.L5
.L10:
	movq	gDevice(%rip), %rax
	movb	$1, 40(%rax)
	call	unpauseAudio
.L5:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE512:
	.size	initAudio, .-initAudio
	.globl	endAudio
	.type	endAudio, @function
endAudio:
.LFB513:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	gDevice(%rip), %rax
	movzbl	40(%rax), %eax
	testb	%al, %al
	je	.L12
	call	pauseAudio
	movq	gDevice(%rip), %rax
	movq	32(%rax), %rax
	movq	%rax, %rdi
	call	freeAudio
	movq	gDevice(%rip), %rax
	movl	(%rax), %eax
	movl	%eax, %edi
	call	SDL_CloseAudioDevice
.L12:
	movq	gDevice(%rip), %rax
	movq	%rax, %rdi
	call	free
	nop
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE513:
	.size	endAudio, .-endAudio
	.globl	pauseAudio
	.type	pauseAudio, @function
pauseAudio:
.LFB514:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	gDevice(%rip), %rax
	movzbl	40(%rax), %eax
	testb	%al, %al
	je	.L15
	movq	gDevice(%rip), %rax
	movl	(%rax), %eax
	movl	$1, %esi
	movl	%eax, %edi
	call	SDL_PauseAudioDevice
.L15:
	nop
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE514:
	.size	pauseAudio, .-pauseAudio
	.globl	unpauseAudio
	.type	unpauseAudio, @function
unpauseAudio:
.LFB515:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	gDevice(%rip), %rax
	movzbl	40(%rax), %eax
	testb	%al, %al
	je	.L18
	movq	gDevice(%rip), %rax
	movl	(%rax), %eax
	movl	$0, %esi
	movl	%eax, %edi
	call	SDL_PauseAudioDevice
.L18:
	nop
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE515:
	.size	unpauseAudio, .-unpauseAudio
	.globl	freeAudio
	.type	freeAudio, @function
freeAudio:
.LFB516:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	%rdi, -24(%rbp)
	jmp	.L20
.L22:
	movq	-24(%rbp), %rax
	movzbl	26(%rax), %eax
	cmpb	$1, %al
	jne	.L21
	movq	-24(%rbp), %rax
	movq	8(%rax), %rax
	movq	%rax, %rdi
	call	SDL_FreeWAV
.L21:
	movq	-24(%rbp), %rax
	movq	%rax, -8(%rbp)
	movq	-24(%rbp), %rax
	movq	64(%rax), %rax
	movq	%rax, -24(%rbp)
	movq	-8(%rbp), %rax
	movq	%rax, %rdi
	call	free
.L20:
	cmpq	$0, -24(%rbp)
	jne	.L22
	nop
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE516:
	.size	freeAudio, .-freeAudio
	.section	.rodata
	.align 8
.LC5:
	.string	"[%s: %d]Warning: filename NULL: %s\n"
.LC6:
	.string	"rb"
	.align 8
.LC7:
	.string	"[%s: %d]Warning: failed to open wave file: %s error: %s\n"
	.text
	.globl	createAudio
	.type	createAudio, @function
createAudio:
.LFB517:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, -56(%rbp)
	movl	%esi, %eax
	movl	%edx, -64(%rbp)
	movb	%al, -60(%rbp)
	movl	$72, %esi
	movl	$1, %edi
	call	calloc
	movq	%rax, -40(%rbp)
	cmpq	$0, -40(%rbp)
	jne	.L24
	movq	stderr(%rip), %rax
	movl	$247, %ecx
	movl	$.LC0, %edx
	movl	$.LC3, %esi
	movq	%rax, %rdi
	movl	$0, %eax
	call	fprintf
	movl	$0, %eax
	jmp	.L25
.L24:
	cmpq	$0, -56(%rbp)
	jne	.L26
	movq	stderr(%rip), %rax
	movq	-56(%rbp), %rdx
	movq	%rdx, %r8
	movl	$253, %ecx
	movl	$.LC0, %edx
	movl	$.LC5, %esi
	movq	%rax, %rdi
	movl	$0, %eax
	call	fprintf
	movl	$0, %eax
	jmp	.L25
.L26:
	movq	-40(%rbp), %rax
	movq	$0, 64(%rax)
	movq	-40(%rbp), %rax
	movzbl	-60(%rbp), %edx
	movb	%dl, 24(%rax)
	movq	-40(%rbp), %rax
	movb	$0, 25(%rax)
	movq	-40(%rbp), %rax
	movb	$1, 26(%rax)
	movl	-64(%rbp), %eax
	movl	%eax, %edx
	movq	-40(%rbp), %rax
	movb	%dl, 27(%rax)
	movq	-40(%rbp), %rax
	leaq	4(%rax), %r13
	movq	-40(%rbp), %rax
	leaq	8(%rax), %r12
	movq	-40(%rbp), %rax
	leaq	32(%rax), %rbx
	movq	-56(%rbp), %rax
	movl	$.LC6, %esi
	movq	%rax, %rdi
	call	SDL_RWFromFile
	movq	%r13, %r8
	movq	%r12, %rcx
	movq	%rbx, %rdx
	movl	$1, %esi
	movq	%rax, %rdi
	call	SDL_LoadWAV_RW
	testq	%rax, %rax
	jne	.L27
	call	SDL_GetError
	movq	%rax, %rcx
	movq	stderr(%rip), %rax
	movq	-56(%rbp), %rdx
	movq	%rcx, %r9
	movq	%rdx, %r8
	movl	$265, %ecx
	movl	$.LC0, %edx
	movl	$.LC7, %esi
	movq	%rax, %rdi
	movl	$0, %eax
	call	fprintf
	movq	-40(%rbp), %rax
	movq	%rax, %rdi
	call	free
	movl	$0, %eax
	jmp	.L25
.L27:
	movq	-40(%rbp), %rax
	movq	8(%rax), %rdx
	movq	-40(%rbp), %rax
	movq	%rdx, 16(%rax)
	movq	-40(%rbp), %rax
	movl	4(%rax), %edx
	movq	-40(%rbp), %rax
	movl	%edx, (%rax)
	movq	-40(%rbp), %rax
	movq	$0, 48(%rax)
	movq	-40(%rbp), %rax
	movq	$0, 56(%rax)
	movq	-40(%rbp), %rax
.L25:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE517:
	.size	createAudio, .-createAudio
	.section	.rodata
	.align 8
.LC8:
	.string	"[%s: %d]Fatal Error: Memory allocation error\n"
	.align 8
.LC9:
	.string	"[%s: %d]Warning: filename and Audio parameters NULL\n"
	.text
	.type	playAudio, @function
playAudio:
.LFB518:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$48, %rsp
	movq	%rdi, -24(%rbp)
	movq	%rsi, -32(%rbp)
	movl	%edx, %eax
	movl	%ecx, -40(%rbp)
	movb	%al, -36(%rbp)
	movq	gDevice(%rip), %rax
	movzbl	40(%rax), %eax
	testb	%al, %al
	je	.L39
	cmpb	$0, -36(%rbp)
	jne	.L31
	movl	gSoundCount(%rip), %eax
	cmpl	$24, %eax
	ja	.L40
	movl	gSoundCount(%rip), %eax
	addl	$1, %eax
	movl	%eax, gSoundCount(%rip)
.L31:
	cmpq	$0, -24(%rbp)
	je	.L33
	movzbl	-36(%rbp), %ecx
	movl	-40(%rbp), %edx
	movq	-24(%rbp), %rax
	movl	%ecx, %esi
	movq	%rax, %rdi
	call	createAudio
	movq	%rax, -8(%rbp)
	jmp	.L34
.L33:
	cmpq	$0, -32(%rbp)
	je	.L35
	movl	$72, %edi
	call	malloc
	movq	%rax, -8(%rbp)
	cmpq	$0, -8(%rbp)
	jne	.L36
	movq	stderr(%rip), %rax
	movl	$313, %ecx
	movl	$.LC0, %edx
	movl	$.LC8, %esi
	movq	%rax, %rdi
	movl	$0, %eax
	call	fprintf
	jmp	.L28
.L36:
	movq	-32(%rbp), %rcx
	movq	-8(%rbp), %rax
	movl	$72, %edx
	movq	%rcx, %rsi
	movq	%rax, %rdi
	call	memcpy
	movl	-40(%rbp), %eax
	movl	%eax, %edx
	movq	-8(%rbp), %rax
	movb	%dl, 27(%rax)
	movq	-8(%rbp), %rax
	movzbl	-36(%rbp), %edx
	movb	%dl, 24(%rax)
	movq	-8(%rbp), %rax
	movb	$0, 26(%rax)
	jmp	.L34
.L35:
	movq	stderr(%rip), %rax
	movl	$325, %ecx
	movl	$.LC0, %edx
	movl	$.LC9, %esi
	movq	%rax, %rdi
	movl	$0, %eax
	call	fprintf
	jmp	.L28
.L34:
	movq	gDevice(%rip), %rax
	movl	(%rax), %eax
	movl	%eax, %edi
	call	SDL_LockAudioDevice
	cmpb	$1, -36(%rbp)
	jne	.L37
	movq	gDevice(%rip), %rax
	movq	32(%rax), %rax
	movq	-8(%rbp), %rdx
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	addMusic
	jmp	.L38
.L37:
	movq	gDevice(%rip), %rax
	movq	32(%rax), %rax
	movq	-8(%rbp), %rdx
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	addAudio
.L38:
	movq	gDevice(%rip), %rax
	movl	(%rax), %eax
	movl	%eax, %edi
	call	SDL_UnlockAudioDevice
	jmp	.L28
.L39:
	nop
	jmp	.L28
.L40:
	nop
.L28:
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE518:
	.size	playAudio, .-playAudio
	.type	addMusic, @function
addMusic:
.LFB519:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	%rdi, -24(%rbp)
	movq	%rsi, -32(%rbp)
	movb	$0, -9(%rbp)
	movq	-24(%rbp), %rax
	movq	64(%rax), %rax
	movq	%rax, -8(%rbp)
	jmp	.L42
.L46:
	movq	-8(%rbp), %rax
	movzbl	24(%rax), %eax
	cmpb	$1, %al
	jne	.L43
	movq	-8(%rbp), %rax
	movzbl	25(%rax), %eax
	testb	%al, %al
	jne	.L43
	cmpb	$0, -9(%rbp)
	je	.L44
	movq	-8(%rbp), %rax
	movl	$0, (%rax)
	movq	-8(%rbp), %rax
	movb	$0, 27(%rax)
.L44:
	movq	-8(%rbp), %rax
	movb	$1, 25(%rax)
	jmp	.L45
.L43:
	movq	-8(%rbp), %rax
	movzbl	24(%rax), %eax
	cmpb	$1, %al
	jne	.L45
	movq	-8(%rbp), %rax
	movzbl	25(%rax), %eax
	cmpb	$1, %al
	jne	.L45
	movb	$1, -9(%rbp)
.L45:
	movq	-8(%rbp), %rax
	movq	64(%rax), %rax
	movq	%rax, -8(%rbp)
.L42:
	cmpq	$0, -8(%rbp)
	jne	.L46
	movq	-32(%rbp), %rdx
	movq	-24(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	addAudio
	nop
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE519:
	.size	addMusic, .-addMusic
	.type	audioCallback, @function
audioCallback:
.LFB520:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$64, %rsp
	movq	%rdi, -40(%rbp)
	movq	%rsi, -48(%rbp)
	movl	%edx, -52(%rbp)
	movq	-40(%rbp), %rax
	movq	%rax, -16(%rbp)
	movq	-16(%rbp), %rax
	movq	%rax, -8(%rbp)
	movb	$0, -21(%rbp)
	movl	-52(%rbp), %eax
	movslq	%eax, %rdx
	movq	-48(%rbp), %rax
	movl	$0, %esi
	movq	%rax, %rdi
	call	SDL_memset
	movq	-16(%rbp), %rax
	movq	64(%rax), %rax
	movq	%rax, -16(%rbp)
	jmp	.L48
.L56:
	movq	-16(%rbp), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	je	.L49
	movq	-16(%rbp), %rax
	movzbl	25(%rax), %eax
	cmpb	$1, %al
	jne	.L50
	movq	-16(%rbp), %rax
	movzbl	24(%rax), %eax
	cmpb	$1, %al
	jne	.L50
	movb	$1, -21(%rbp)
	movq	-16(%rbp), %rax
	movzbl	27(%rax), %eax
	testb	%al, %al
	je	.L51
	movq	-16(%rbp), %rax
	movzbl	27(%rax), %eax
	leal	-1(%rax), %edx
	movq	-16(%rbp), %rax
	movb	%dl, 27(%rax)
	jmp	.L50
.L51:
	movq	-16(%rbp), %rax
	movl	$0, (%rax)
.L50:
	cmpb	$0, -21(%rbp)
	je	.L52
	movq	-16(%rbp), %rax
	movzbl	24(%rax), %eax
	cmpb	$1, %al
	jne	.L52
	movq	-16(%rbp), %rax
	movzbl	25(%rax), %eax
	testb	%al, %al
	jne	.L52
	movl	$0, -20(%rbp)
	jmp	.L53
.L52:
	movq	-16(%rbp), %rax
	movl	(%rax), %edx
	movl	-52(%rbp), %eax
	cmpl	%eax, %edx
	cmovbe	%edx, %eax
	movl	%eax, -20(%rbp)
.L53:
	movq	-16(%rbp), %rax
	movzbl	27(%rax), %eax
	movzbl	%al, %ecx
	movl	-20(%rbp), %edx
	movq	-16(%rbp), %rax
	movq	16(%rax), %rsi
	movq	-48(%rbp), %rax
	movl	%ecx, %r8d
	movl	%edx, %ecx
	movl	$32784, %edx
	movq	%rax, %rdi
	call	SDL_MixAudioFormat
	movq	-16(%rbp), %rax
	movq	16(%rax), %rdx
	movl	-20(%rbp), %eax
	cltq
	addq	%rax, %rdx
	movq	-16(%rbp), %rax
	movq	%rdx, 16(%rax)
	movq	-16(%rbp), %rax
	movl	(%rax), %edx
	movl	-20(%rbp), %eax
	subl	%eax, %edx
	movq	-16(%rbp), %rax
	movl	%edx, (%rax)
	movq	-16(%rbp), %rax
	movq	%rax, -8(%rbp)
	movq	-16(%rbp), %rax
	movq	64(%rax), %rax
	movq	%rax, -16(%rbp)
	jmp	.L48
.L49:
	movq	-16(%rbp), %rax
	movzbl	24(%rax), %eax
	cmpb	$1, %al
	jne	.L54
	movq	-16(%rbp), %rax
	movzbl	25(%rax), %eax
	testb	%al, %al
	jne	.L54
	movq	-16(%rbp), %rax
	movq	8(%rax), %rdx
	movq	-16(%rbp), %rax
	movq	%rdx, 16(%rax)
	movq	-16(%rbp), %rax
	movl	4(%rax), %edx
	movq	-16(%rbp), %rax
	movl	%edx, (%rax)
	jmp	.L48
.L54:
	movq	-16(%rbp), %rax
	movq	64(%rax), %rdx
	movq	-8(%rbp), %rax
	movq	%rdx, 64(%rax)
	movq	-16(%rbp), %rax
	movzbl	24(%rax), %eax
	testb	%al, %al
	jne	.L55
	movl	gSoundCount(%rip), %eax
	subl	$1, %eax
	movl	%eax, gSoundCount(%rip)
.L55:
	movq	-16(%rbp), %rax
	movq	$0, 64(%rax)
	movq	-16(%rbp), %rax
	movq	%rax, %rdi
	call	freeAudio
	movq	-8(%rbp), %rax
	movq	64(%rax), %rax
	movq	%rax, -16(%rbp)
.L48:
	cmpq	$0, -16(%rbp)
	jne	.L56
	nop
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE520:
	.size	audioCallback, .-audioCallback
	.type	addAudio, @function
addAudio:
.LFB521:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	cmpq	$0, -8(%rbp)
	je	.L62
	jmp	.L60
.L61:
	movq	-8(%rbp), %rax
	movq	64(%rax), %rax
	movq	%rax, -8(%rbp)
.L60:
	movq	-8(%rbp), %rax
	movq	64(%rax), %rax
	testq	%rax, %rax
	jne	.L61
	movq	-8(%rbp), %rax
	movq	-16(%rbp), %rdx
	movq	%rdx, 64(%rax)
	jmp	.L57
.L62:
	nop
.L57:
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE521:
	.size	addAudio, .-addAudio
	.ident	"GCC: (Ubuntu 5.4.0-6ubuntu1~16.04.11) 5.4.0 20160609"
	.section	.note.GNU-stack,"",@progbits
