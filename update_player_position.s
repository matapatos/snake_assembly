.globl	SDL_QUIT
.type	SDL_QUIT, @object
.size	SDL_QUIT, 4
.globl	SDL_KEYDOWN
.type	SDL_KEYDOWN, @object
.size	SDL_KEYDOWN, 4
.globl	SDLK_ESCAPE
.type	SDLK_ESCAPE, @object
.size	SDLK_ESCAPE, 4
SDL_QUIT:
    .int 256
SDL_KEYDOWN:
    .int 768
SDLK_UP:
    .int 1073741906
SDLK_DOWN:
    .int 1073741905
SDLK_RIGHT:
    .int 1073741903
SDLK_LEFT:
    .int 1073741904
SDLK_ESCAPE:
    .int 27
ERROR_Invalid_Direction:
    .asciz "Invalid direction value, given: %d\n"
.section .text
.type update_player_position, @function
.globl update_player_position
/**
Update player position (e.g. X and Y) regarding its direction.

NOTE: It does not required any input or output. It automatically
      uses the Player' properties.

Pseudo-code:
    for(int i = Player.num_parts - 1; i > 0; i--) {
        next_part = Player.parts[i - 1];
        Player.parts[i].x = next_part.x;
        Player.parts[i].y = next_part.y;
    }

    // Update Player direction if needed
    Player.direction = get_direction_regarding_keyboard_events();

    switch(Player.direction) {
        case UP:
            Player.parts[0].y -= 1;
            break;
        
        case DOWN:
            Player.parts[0].y += 1;
            break;
        
        case RIGHT:
            Player.parts[0].x += 1;
            break;
        
        case LEFT:
            Player.parts[0].x -= 1;
            break;
    }
**/
update_player_position:
    call    .handle_keyevents
.update_parts_position:
    pushq   %rax
    # Loop through parts
        # -> Update their position according to their parents' position
    movl    -972(%rbp), %edi  # Get num of parts
    decl    %edi
    cmpl    $0, %edi
    je      .update_head_of_snake_position  # Skip parts update position in case that only the tail exists
    leaq    -968(%rbp), %rsi  # Get remory reference for the first element
    # Calculate start index
    movq    $0, %rax
    movl    $16, %eax
    mull    %edi
    addq    %rax, %rsi
    .parts_loop:
        movl    -16(%rsi), %eax
        movl    %eax, 0(%rsi)
        movl    -12(%rsi), %eax
        movl    %eax, 4(%rsi)
        movl    RECT_SIZE, %eax
        movl    %eax, 8(%rsi)
        movl    %eax, 12(%rsi)
        # Update pointer index to one structure less
        subq    $16, %rsi
        # For loop condition: %eax--; %eax > 0
        decl    %edi 
        cmpl    $0, %edi
        jne     .parts_loop

.update_head_of_snake_position:
    popq    %rax
    .direction_up:
        movl    DIRECTION_UP, %eax
        cmpl    %eax, -980(%rbp)
        jne     .direction_down
        movl    RECT_SIZE+4, %eax
        subl    %eax, -964(%rbp)  # Decrement Y position
        ret
    .direction_down:
        movl    DIRECTION_DOWN, %eax
        cmpl    %eax, -980(%rbp)
        jne     .direction_left
        movl    RECT_SIZE+4, %eax
        addl    %eax, -964(%rbp)  # Increment Y position
        ret
    .direction_left:
        movl    DIRECTION_LEFT, %eax
        cmpl    %eax, -980(%rbp)
        jne     .direction_right
        movl    RECT_SIZE, %eax
        subl    %eax, -968(%rbp)  # Decrement X position
        ret
    .direction_right:
        movl    DIRECTION_RIGHT, %eax
        cmpl    %eax, -980(%rbp)
        jne     .direction_error
        movl    RECT_SIZE, %eax
        addl    %eax, -968(%rbp)  # Increment X position
        ret
    .direction_error:
        movl    -980(%rbp), %esi
        movl    $ERROR_Invalid_Direction, %edi
        movl    $0, %eax
        call    SDL_Log
        call    release_memory  # Shutdown game

.handle_keyevents:
    .init:
        movl    -980(%rbp), %edx  # New direction
    .loop_keyevents:
        pushq   %rdx
        leaq    -1136(%rbp), %rdi
        call    SDL_PollEvent
        popq    %rdx
        cmpl    $0, %eax
        je      .return
        movl    SDL_KEYDOWN, %eax
        cmpl    %eax, -1136(%rbp)
        jne     .check_quit_event
        .check_key_up:
            # If previous direction was DOWN snake cannot turn UP!
            movl    DIRECTION_DOWN, %eax
            cmpl    %eax, -980(%rbp)
            je      .is_key_down
            .is_key_up:
                movl    SDLK_UP, %eax
                cmpl    %eax, -1116(%rbp)
                jne     .check_key_down
                movl    DIRECTION_UP, %edx  # Add SDLK_UP code
                jmp     .loop_keyevents
        .check_key_down:
            # If previous direction was UP snake cannot turn DOWN!
            movl    DIRECTION_UP, %eax
            cmpl    %eax, -980(%rbp)
            je      .is_key_left
            .is_key_down:
                movl    SDLK_DOWN, %eax
                cmpl    %eax, -1116(%rbp)
                jne     .check_key_left
                movl    DIRECTION_DOWN, %edx  # Add SDLK_DOWN code
                jmp     .loop_keyevents
        .check_key_left:
            # If previous direction was RIGHT snake cannot turn LEFT!
            movl    DIRECTION_RIGHT, %eax
            cmpl    %eax, -980(%rbp)
            je      .is_key_right
            .is_key_left:
                movl    SDLK_LEFT, %eax
                cmpl    %eax, -1116(%rbp)
                jne     .check_key_right
                movl    DIRECTION_LEFT, %edx  # Add SDLK_LEFT code
                jmp     .loop_keyevents
        .check_key_right:
            # If previous direction was RIGHT snake cannot turn LEFT!
            movl    DIRECTION_LEFT, %eax
            cmpl    %eax, -980(%rbp)
            je      .check_escape_event
            .is_key_right:
                movl    SDLK_RIGHT, %eax
                cmpl    %eax, -1116(%rbp)
                jne     .check_escape_event
                movl    DIRECTION_RIGHT, %edx  # Add SDLK_RIGHT code
                jmp     .loop_keyevents
        .check_escape_event:
            movl    SDLK_ESCAPE, %eax
            cmpl    %eax, -1116(%rbp)
            jne     .loop_keyevents
            movl    $0, -1008(%rbp)  # Set game_state to 0|False
            ret
        .check_quit_event:
            movl    SDL_QUIT, %eax
            cmpl    %eax, -1136(%rbp)
            jne     .loop_keyevents
            movl    $0, -1008(%rbp)  # Set game_state to 0|False
        .return:
            movl    %edx, -980(%rbp)  # Update new direction
            ret
