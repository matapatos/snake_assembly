.section .data
.globl	game_state
.type	game_state, @object
.size	game_state, 4
.globl	GAME_BORDERS
.type	GAME_BORDERS, @object
.size	GAME_BORDERS, 8
.globl	RECT_SIZE
.type	RECT_SIZE, @object
.size	RECT_SIZE, 8
.globl	PLAYER_MAX_PARTS
.type	PLAYER_MAX_PARTS, @object
.size	PLAYER_MAX_PARTS, 4
.globl	DIRECTION_UP
.type	DIRECTION_UP, @object
.size	DIRECTION_UP, 4
.globl	DIRECTION_DOWN
.type	DIRECTION_DOWN, @object
.size	DIRECTION_DOWN, 4
.globl	DIRECTION_LEFT
.type	DIRECTION_LEFT, @object
.size	DIRECTION_LEFT, 4
.globl	DIRECTION_RIGHT
.type	DIRECTION_RIGHT, @object
.size	DIRECTION_RIGHT, 4
.globl	SDL_CreateWindow_args
.type	SDL_CreateWindow_args, @object
.size	SDL_CreateWindow_args, 20
.type release_memory, @function
.globl release_memory
SDL_Init_arg:
    .int 48  # SDL_INIT_VIDEO | SDL_INIT_AUDIO
             #
             # SDL_INIT_VIDEO = 32;
             # SDL_INIT_AUDIO = 16;
             #
             # So, 32 + 16 = 48
GAME_WINDOW_NAME:
    .asciz "Assembly-Snake"
SDL_CreateWindow_args:
    .int 2, 600, 800, 536805376, 536805376  # Reverse ordering of calling function
SDL_CreateRenderer_args:
    .int 0, -1
# -------------- Define Game variables ----------------
GAME_BORDERS:
        # X, Y
    .int 39, 29
PLAYER_MAX_PARTS:
    .int 53
PLAYER_INIT_POSITION:
        # X, Y
    .int 400, 300
RECT_SIZE:
    #    w,  h
    .int 20, 20
FPS:
    .int 20
DIRECTION_UP:
    .int 1
DIRECTION_DOWN:
    .int 2
DIRECTION_LEFT:
    .int 3
DIRECTION_RIGHT:
    .int 4
TTF_FONT_FILEPATH:
    .asciz "font/PixelFJVerdana12pt.ttf"
GAME_MUSIC:
    .asciz "./sounds/background.wav"
# -------------- End defining game vars ---------------
# Define error messages
ERROR_SDL_Init:
    .asciz "Error initializing SDL\n"
ERROR_SDL_CreateWindow:
    .asciz "Error creating window\n"
ERROR_SDL_CreateRenderer:
    .asciz "Error creating renderer\n"
ERROR_TTF_OpenFont:
    .asciz "Coudn't find the given font file '%s'\n"
ERROR_TTF_Init:
    .asciz "Error initializing TTF"
.section .text
.globl main
main:
nop
.init_sdl:
    #### Copied instructions from C compiler ####
    pushq   %rbp
    movq    %rsp, %rbp
    subq    $2080, %rsp  # ~2KB for the game properties, like snake size and all
    movq    %fs:40, %rax
    movq    %rax, -8(%rbp)
    xorl    %eax, %eax
    #### End of copied section ####
    movl    $48, %edi
    call    SDL_Init
    testl   %eax, %eax  # Check if SDL have been initialized properly
    je      .create_sdl_window
    movl    $ERROR_SDL_Init, %edi
    movl    $0, %eax
    call    SDL_Log
    call    .close_program
.create_sdl_window:
    movl    SDL_CreateWindow_args, %r9d
    movl    SDL_CreateWindow_args+4, %r8d
    movl    SDL_CreateWindow_args+8, %ecx
    movl    SDL_CreateWindow_args+12, %edx
    movl    SDL_CreateWindow_args+16, %esi
    movl    $GAME_WINDOW_NAME, %edi
    call    SDL_CreateWindow
    movq    %rax, -96(%rbp)  # Check return value: if(window == NULL) ERROR!
    cmpq    $0, -96(%rbp)
    jne     .create_renderer
    movl    $ERROR_SDL_CreateWindow, %edi
    movl    $0, %eax
    call    SDL_Log
    call    .close_program
.create_renderer:
    movq    -96(%rbp), %rax
    movl    SDL_CreateRenderer_args, %edx
    movl    SDL_CreateRenderer_args+4, %esi
    movq    %rax, %rdi
    call    SDL_CreateRenderer
    movq    %rax, -88(%rbp)
    cmpq    $0, -88(%rbp)  # Check return value: if(renderer == NULL) ERROR!
    jne     .init_TTF
    movl    $ERROR_SDL_CreateRenderer, %edi
    call    SDL_Log
    call    .destroy_window
.init_TTF:
    call    TTF_Init
    cmpq    $0, %rax
    je      .open_TTF
    movl    $ERROR_TTF_Init, %edi
    movl    $0, %eax
    call    SDL_Log
    call    .destroy_renderer
    .open_TTF:
        call    SDL_GetBasePath
        movq    $TTF_FONT_FILEPATH, %rsi
        movq    %rax, %rdi
        call    concat
        movq     %rax, -2064(%rbp)  # Filename pointer
        movl    $30, %esi
        movq    -2064(%rbp), %rdi
        call    TTF_OpenFont
        movq    %rax, -2056(%rbp) # TTF_Font pointer
        cmpq    $0, %rax
        jne     .init_font_color
        movq    -2064(%rbp), %rsi
        movl    $ERROR_TTF_OpenFont, %edi
        movl    $0, %eax
        call    SDL_Log
        call    .close_ttf
    .init_font_color:
        # SDL_Color
        movb    $127, -2048(%rbp)
        movb    $127, -2047(%rbp)
        movb    $30, -2046(%rbp)
        movb    $127, -2045(%rbp)
.init_audio:
    call    initAudio  # Init audio from external C library
.play_music:
    movl    $128, %esi  # Max volume
    movl    $GAME_MUSIC, %edi
    call    playMusic
.starting_screen:
    call    waiting_screen
.init_game:
    movl    $1, -1008(%rbp)  # Set game_state to 1|True
    movl    $1000, -1004(%rbp)  # Set var for checking max FPS
    movl    FPS, %eax
    subl    %eax, -1004(%rbp)  # Set time regarding FPS
    # Calculate milliseconds to delay regarding FPS
    movl    $1000, %eax
    movl    FPS, %esi
    cltd
    idivl   %esi
    movl    %eax, -1080(%rbp)
    # Seed and Player properties
    call    randomly_init_seed  # X = -996(%rbp) && Y = -992(%rbp) && W = -988(%rbp) && H = -984(%rbp)
    movl    DIRECTION_UP, %eax
    movl    %eax, -980(%rbp)  # Set Player direction
    movl    $0, -976(%rbp)  # Set Player points
    movl    $1, -972(%rbp)  # Set Player num parts
    movl    PLAYER_INIT_POSITION, %eax
    movl    %eax, -968(%rbp)  # Set Player X position
    movl    PLAYER_INIT_POSITION+4, %eax
    movl    %eax, -964(%rbp)  # Set Player Y position
    movl    RECT_SIZE, %eax
    movl    %eax, -960(%rbp)
    movl    %eax, -956(%rbp)
.game_loop:
    # Get starting ticks
    call    SDL_GetTicks
    movl    %eax, -1000(%rbp)

    call    update_seed_location
    call    update_player_position
    call    check_collisions
    call    update_view
    
    # Check FPS
    call    SDL_GetTicks
    subl    -1000(%rbp), %eax  # end_time - starting_time
    cmpl    -1004(%rbp), %eax
    ja      .check_game_state  # if -1004(%rbp) > %eax
    # 1000/FPS - (end_time - starting_time)
    movl    -1080(%rbp), %edi
    subl    %eax, %edi
    call    SDL_Delay
    .check_game_state:
        cmpl    $1, -1008(%rbp)  # Check if game state is 0
        je      .game_loop
        call    game_over
release_memory:
    .end_audio:
        call    endAudio  # Release audio from external C library
    .close_ttf_font:
        movq    -2056(%rbp), %rdi
        call    TTF_CloseFont
    .close_ttf:
        call    TTF_Quit
    .destroy_renderer:
        movq    -88(%rbp), %rdi
        call    SDL_DestroyRenderer
    .destroy_window:
        movq    -96(%rbp), %rdi
        call    SDL_DestroyWindow
    .close_program:
        call    SDL_Quit
        # Exit program with return value of 0
        push    $0
        call    exit
