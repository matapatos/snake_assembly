GAME_OVER_MUSIC:
    .asciz "./sounds/game_over.wav"
IMAGE_PATH:
    .asciz "./images/game_over.png"
SCORE_FILEPATH:
    .asciz "best_score.txt"
CURRENT_SCORE_STRING:
    .asciz "Current score:"
BEST_SCORE_STRING:
    .asciz "Best score:"

.section .text
.type game_over, @function
.globl game_over
game_over:
.play_sound:
    movl    $128, %esi  # Max volume
    movl    $GAME_OVER_MUSIC, %edi
    call    playSound
.show_game_over_image:
    leaq    IMAGE_PATH, %rdi
    call    render_png_image
.show_scores:
    .font_color:
        # SDL_Color
        movb    $127, -2048(%rbp)
        movb    $127, -2047(%rbp)
        movb    $127, -2046(%rbp)
        movb    $127, -2045(%rbp)

    .show_current_score:
        .current_score_draw_text:
            .current_score_text_font_rect:
                movl    $180, -2044(%rbp)
                movl    $480, -2040(%rbp)
                movl    $284, -2036(%rbp)
                movl    $40, -2032(%rbp)

            movq    -2056(%rbp), %r8  # TTF_Font
            leaq    -2048(%rbp), %rcx  # SDL_Color
            leaq    -2044(%rbp), %rdx  # SDL_Rect
            leaq    CURRENT_SCORE_STRING, %rsi
            movq    -88(%rbp), %rdi    # SDL_Renderer
            call    render_text
        .current_score_draw:
            .current_score_font_rect:
                movl    $520, -2044(%rbp)
                movl    $480, -2040(%rbp)
                movl    $40, -2036(%rbp)
                movl    $40, -2032(%rbp)
                cmpl    $9, -976(%rbp)
                jg      .current_score_render_int
                movl    $30, -2036(%rbp)
            .current_score_render_int:
                movq    -2056(%rbp), %r8  # TTF_Font
                leaq    -2048(%rbp), %rcx  # SDL_Color
                leaq    -2044(%rbp), %rdx  # SDL_Rect
                movl    -976(%rbp), %esi   # int score
                movq    -88(%rbp), %rdi    # SDL_Renderer
                call    render_int

    .show_best_score:
        leaq    SCORE_FILEPATH, %rdi
        call    read_best_score
        pushq   %rax
        cmpl    %eax, -976(%rbp)
        jl      .best_score_draw_text
        .write_new_best_score:
            movl    -976(%rbp), %esi
            leaq    SCORE_FILEPATH, %rdi
            call    write_new_best_score
        .best_score_draw_text:
            .best_score_text_font_rect:
                movl    $180, -2044(%rbp)
                movl    $520, -2040(%rbp)
                movl    $213, -2036(%rbp)
                movl    $40, -2032(%rbp)

            movq    -2056(%rbp), %r8  # TTF_Font
            leaq    -2048(%rbp), %rcx  # SDL_Color
            leaq    -2044(%rbp), %rdx  # SDL_Rect
            leaq    BEST_SCORE_STRING, %rsi
            movq    -88(%rbp), %rdi    # SDL_Renderer
            call    render_text
        .best_score_draw:
            .best_score_font_rect:
                movl    $520, -2044(%rbp)
                movl    $520, -2040(%rbp)
                movl    $40, -2036(%rbp)
                movl    $40, -2032(%rbp)
                popq    %rsi
                cmpl    $9, %esi
                jg      .best_score_render_int
                movl    $30, -2036(%rbp)
            .best_score_render_int:
                movq    -2056(%rbp), %r8  # TTF_Font
                leaq    -2048(%rbp), %rcx  # SDL_Color
                leaq    -2044(%rbp), %rdx  # SDL_Rect
                # %esi == best_score
                movq    -88(%rbp), %rdi    # SDL_Renderer
                call    render_int

    .render_updated_view:
        movq    -88(%rbp), %rdi
        call    SDL_RenderPresent    

.delay:
    movl $0, %eax
    movl $5000, %edi    
    call SDL_Delay

    ret
