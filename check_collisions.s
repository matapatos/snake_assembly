.section .text
.type check_collisions, @function
.globl check_collisions
/**
Check if Player crossed the Game borders as well as if it collided with himself.
**/
check_collisions:
    call    .check_borders
    call    .check_player
    ret
/**
Check if Player crossed the Game borders.

NOTE: It does not require/have any input or output. It automatically
      uses the Player properties (e.g. X and Y position) and the Game
      properties (e.g. width and height).
**/
.check_borders:  # Check borders collisions
    movl    -968(%rbp), %eax
    cmpl    $0, %eax
    jl      .set_game_over
    movl    SDL_CreateWindow_args+8, %edi
    subl    RECT_SIZE, %edi
    cmpl    %edi, %eax
    jg      .set_game_over
    movl    -964(%rbp), %eax
    cmpl    $0, %eax
    jl      .set_game_over
    movl    SDL_CreateWindow_args+4, %edi
    subl    RECT_SIZE, %edi
    cmpl    %edi, %eax
    jg      .set_game_over
    ret
/**
Check if Player collided with himself.

NOTE: It does not require/have any input or output. It automatically
      uses the Player properties (e.g. X and Y positions of each part).
**/
.check_player:  # Check player collision with himself
    # Loop through parts
        # -> Update their position according to their parents' position
    cmpl    $2, -972(%rbp)
    jg      .check_player_collisions
    ret
    .check_player_collisions:
        leaq    -968(%rbp), %rsi  # Get remory reference for the first element
        movl    -968(%rbp), %r9d  # Hold X position of head
        movl    -964(%rbp), %r8d  # Hold Y position of head
        movl    $1, %edi
        addq    $16, %rsi
        .parts_loop:
            cmpl    0(%rsi), %r9d
            jne     .update_index
            cmpl    4(%rsi), %r8d
            jne     .update_index
            call    .set_game_over
            ret
            .update_index:
                # Update pointer index to one structure less
                addq    $16, %rsi
                # For loop condition: %eax--; %eax > 0
                incl    %edi 
                cmpl    -972(%rbp), %edi
                jne     .parts_loop
                ret

.set_game_over:
    movl    $0, -1008(%rbp)
    ret
