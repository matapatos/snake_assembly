IMAGE_TYPE_PNG:
    .int 2
FILE_BINARY_APPEND_MODE:
	.asciz "ab+"
FILE_BINARY_WRITE_MODE:
	.asciz "wb"
READ_UNTIL_END_OF_LINE_OR_EOF:
	.asciz "%[^\n]"
READ_INT:
	.asciz "%d"
WRITE_INT:
	.asciz "%d"
### Errors msg section ###
ERROR_IMG_Init:
    .asciz "Error initialiing IMG_Init"
ERROR_IMG_Load:
    .asciz "Error loading image with IMG_Load"
ERROR_SDL_CreateTextureFromSurface:
    .asciz "Error when calling SDL_CreateTextureFromSurface"
ERROR_OPENING_FILE:
	.asciz "Error opening file '%s'"
ERROR_WRITING_TO_FILE:
	.asciz "Error writing to file '%s'"
ERROR_TTF_RenderText_Solid:
    .asciz "Error using TTF_RenderText_Solid\n"

.section .text
.type concat, @function
.globl concat
/**
Concatenate two strings.

inputs:
	%rdi - Pointer to the first string to concatenate
	%rsi - Pointer to the second string to concatenate

output:
	%rax - Pointer to the concatenated string

Pseudo-code: (adapted from https://stackoverflow.com/questions/8465006/how-do-i-concatenate-two-strings-in-c#answer-8465083)
	char* concat(const char *s1, const char *s2)
	{
		char *result = malloc(strlen(s1) + strlen(s2) + 1); // +1 for the null-terminator
		if(result == NULL)
			release_memory();
		else {
			strcpy(result, s1);
			strcat(result, s2);
		}
		return result;
	}
**/
concat:
	pushq 	%rbp
	movq 	%rsp, %rbp
	subq 	$32, %rsp

	movq 	%rdi, -32(%rbp)  # s1
	movq 	%rsi, -24(%rbp)  # s2
	call 	strlen
	movq 	%rax, -16(%rbp)  # len(s1)
	movq 	-24(%rbp), %rdi
	call 	strlen
	addq 	-16(%rbp), %rax
	addq 	$1, %rax  # For null termination
	movq 	%rax, %rdi
	call 	malloc
	movq 	%rax, -8(%rbp)
	cmpq	$0, -8(%rbp)
	jne		.copy_strings
	call	release_memory
	.copy_strings:
		movq 	-32(%rbp), %rsi  # s1
		movq 	-8(%rbp), %rdi
		call 	strcpy
		movq 	-24(%rbp), %rsi  # s2
		movq 	-8(%rbp), %rdi
		addq 	-16(%rbp), %rdi
		call 	strcpy
		movq 	-8(%rbp), %rax  # Return value

	addq 	$32, %rsp
	popq 	%rbp
	ret

.section .text
.type inclusive_random_range, @function
.globl inclusive_random_range
/**
Inclusive random number.

inputs:
	%edi - Minimum int value (inclusive)
	%esi - Maximum int value (inclusive)
outputs:
	%eax - int value in range between %edi and %esi

Function pseudo-code:
	int inclusive_random_range(min_number, max_number) {
 		return rand() % (max_number + 1 - min_number) + min_number;
	}
**/
inclusive_random_range:
	# 
	pushq 	%rbp
	movq 	%rsp, %rbp
	subq 	$16, %rsp
	movl 	%edi, -4(%rbp)
	movl 	%esi, -8(%rbp)
	call 	rand
	movl 	%eax, %edx
	movl 	-8(%rbp), %eax
	addl 	$1, %eax
	subl 	-4(%rbp), %eax
	movl 	%eax, %ecx
	movl 	%edx, %eax
	cltd
	idivl 	%ecx
	movl 	-4(%rbp), %eax
	addl 	%edx, %eax
	leave
	ret

.section .text
.type render_png_image, @function
.globl render_png_image
/**
Renders a PNG image to the screen.

inputs:
	%rdi - Pointer to the image filepath to be rendered.

Pseudo-code:
    void waiting_screen(char *image_filepath) {
        global SDL_Renderer *renderer;

        if(IMG_Init(() == NULL) {
            SDL_Log(ERROR);
            release_memory();
        }

        SDL_Surface *image = IMG_Load(image_filepath);
        if(image == NULL) {
            SDL_Log(ERROR);
            release_memory();
        }

        SDL_Texture *texture = SDL_CreateTextureFromSurface(renderer, image);
        if(texture == NULL) {
            SDL_Log(ERROR);
            release_memory();
        }

		// Free unnecessary objects
        SDL_FreeSurface(image);

        // Render image to screen
		SDL_RenderClear(renderer);
		SDL_RenderCopy(renderer, texture);
		SDL_RenderPresent(renderer);
		
		IMG_Quit();
	}
**/
render_png_image:
	pushq 	%rdi
	movl    $IMAGE_TYPE_PNG, %edi
    movl    $0, %eax
    call    IMG_Init  # IMG_Init(IMAGE_TYPE_PNG)
    cmpl    $0, %eax
    jne     .load_image
    movl    $ERROR_IMG_Init, %edi
	movl    $0, %eax
	call    SDL_Log
    call    release_memory
    .load_image:
		popq	%rdi
        call    IMG_Load
        movq    %rax, -80(%rbp)  # image* = IMG_Load(...)
        cmpq    $0, -80(%rbp)
        jne     .create_texture_from_surface
        movl    $ERROR_IMG_Load, %edi
        movl    $0, %eax
        call    SDL_Log
        call    IMG_Quit
        call    release_memory
    .create_texture_from_surface:
        movq    -80(%rbp), %rsi
        movq    -88(%rbp), %rdi
        call    SDL_CreateTextureFromSurface
        movq    %rax, -72(%rbp)
        cmpq    $0, %rax
        jne     .free_surface
        movl    $ERROR_SDL_CreateTextureFromSurface, %edi
        movl    $0, %eax
        call    SDL_Log
        call    IMG_Quit
        call    release_memory
    .free_surface:
        movq    -80(%rbp), %rdi
        call    SDL_FreeSurface
    .render_image:
			movl    $255, %r8d
			movl    $0, %ecx
			movl    $0, %edx
			movl    $0, %esi
			movq    -88(%rbp), %rdi
			call    SDL_SetRenderDrawColor
            movq    -88(%rbp), %rdi
            call    SDL_RenderClear
            movl    $0, %ecx
            movl    $0, %edx
            movq    -72(%rbp), %rsi
            movq    -88(%rbp), %rdi
            call    SDL_RenderCopy
            movq    -88(%rbp), %rdi
            call    SDL_RenderPresent
			call 	IMG_Quit
			ret

.section .text
.type read_best_score, @function
.globl read_best_score
/**
Reads best score from a file and converts it to an int.

inputs:
	%rdi - Pointer to the filepath best score.

outputs:
	%eax - Int value with the best score. If no previous score was set, it returns 0.

Pseudo-code:
	int read_best_score(char *best_score_filepath) {
		int best_score = 0;
		char score_text[6];  // It's size 6 because the maximum int number has 5 digits plus one for the null

		FILE *fptr;
		if ((fptr = fopen(best_score_filepath, FILE_BINARY_APPEND_MODE)) == NULL)
		{
			SDL_Log(ERROR);
			release_memory();    
		}

		// Reads until newline or EOF 
    	int result = fscanf(fptr,"%[^\n]", score_text);
		if(result <= -1) {  # Means that file is empty
			fclose(fptr);
			return best_score;
		}

		sscanf(score_text, "%d", &best_score);
		fclose(fptr);

		return best_score;
	}
**/
read_best_score:
	pushq	%rbp
	movq	%rsp, %rbp
	subq	$34, %rsp

	movl 	$0, -24(%rbp)  # Set score to 0, in case that file is empty
	movq	%rdi, -8(%rbp)  # Save filename to Stack
	movl	$FILE_BINARY_APPEND_MODE, %esi  # Its Append mode instead of just Read
											# because we want to create the file if
											# doesnt exists
	call	fopen
	movq	%rax, -16(%rbp)
	cmpq	$0, -16(%rbp)
	jne		.read
	movq 	-8(%rbp), %rsi
	movl	$ERROR_OPENING_FILE, %edi
	movq	$0, %rax
	call	SDL_Log
	call	release_memory
	.read:
        movl    $0, -28(%rbp)
        movb    $0, -32(%rbp)
        movb    $0, -33(%rbp)
		leaq	-28(%rbp), %rdx
		movl	$READ_UNTIL_END_OF_LINE_OR_EOF, %esi
		movq	-16(%rbp), %rdi
		movl	$0, %eax
		call 	fscanf
		cmpl	$-1, %eax
		jle		.return_read_best_score
	.convert_data_to_int:
		leaq	-24(%rbp), %rdx
		movl	$READ_INT, %esi
		leaq 	-28(%rbp), %rdi
		movl 	$0, %eax
		call 	sscanf
	.return_read_best_score:
		# Means that file is empty
		movq	-16(%rbp), %rdi
		call	fclose
		movl	-24(%rbp), %eax
	
	addq	$34, %rsp
	popq	%rbp
	ret

.section .text
.type write_new_best_score, @function
.globl write_new_best_score
/**
Writes best score to a file.

inputs:
	%esi - Int score to write to file.
	%rdi - Pointer to the filepath best score.

Pseudo-code:
	void write_new_best_score(char *best_score_filepath, int score) {
		FILE *fptr;
		if ((fptr = fopen(best_score_filepath, FILE_BINARY_APPEND_MODE)) == NULL)
		{
			SDL_Log(ERROR);
			release_memory();    
		}

		// Convert int to string
		if(fprintf(fptr, "%d", score) < 0) {
			SDL_Log(ERROR);
		}
  			
		fclose(fptr);
	}
**/
write_new_best_score:
	pushq	%rbp
	movq	%rsp, %rbp
	subq	$24, %rsp

	movl 	%esi, -24(%rbp)  # Save int score
	movq	%rdi, -8(%rbp)  # Save filename to Stack
	movl	$FILE_BINARY_WRITE_MODE, %esi
	call	fopen
	movq	%rax, -16(%rbp)
	cmpq	$0, -16(%rbp)
	jne		.write
	movq 	-8(%rbp), %rsi
	movl	$ERROR_OPENING_FILE, %edi
	movq	$0, %rax
	call	SDL_Log
	call	release_memory
	.write:
		movl 	-24(%rbp), %edx
		movl	$WRITE_INT, %esi
		movq	-16(%rbp), %rdi
		movl	$0, %eax
		call 	fprintf
		cmpl	$-1, %eax
		jg		.close_file
		movq	-8(%rbp), %rsi
		movl 	$ERROR_WRITING_TO_FILE, %edi
		movl	$0, %eax
		call	SDL_Log
	.close_file:
		movq	-16(%rbp), %rdi
		call	fclose
	
	addq	$24, %rsp
	popq	%rbp
	ret

.section .text
.type render_int, @function
.globl render_int
/**
Renders int to the screen.

inputs:
	%r8d - Pointer to the TTF_Font.
	%rcx - Pointer to the SDL_Color for the text.
	%rdx - Pointer to the SDL_Rect where to render the text (i.e. location and size 
																  in the screen of 
																  the text).
	%esi - Int to render.
	%rdi - Pointer to SDL_Renderer.

Pseudo-code:
	void render_int(SDL_Renderer *renderer, int num, SDL_Rect *rect, SDL_Color *color, TTF_Font *font) {
		char num_str[8];
		SDL_itoa(num, &num_str[0], 10);
		render_text(renderer, &num_str[0], rect, color, font);
	}
**/
render_int:
	pushq	%rbp
	movq	%rsp, %rbp
	subq	$40, %rsp

	movq	%r8, -32(%rbp)  # TTF_Font
	movq	%rcx, -24(%rbp)
	movq	%rdx, -16(%rbp)
	movq	%rdi, -8(%rbp)

    .convert_int_to_string:
        movl    $10, %edx
        movl    %esi, %edi
        leaq    -40(%rbp), %rsi # Points as char
        call    SDL_itoa
	.render_string:
		movq	-32(%rbp), %r8
		movq	-24(%rbp), %rcx
		movq 	-16(%rbp), %rdx
		leaq    -40(%rbp), %rsi
		movq 	-8(%rbp), %rdi
		call 	render_text

	addq	$40, %rsp
	popq	%rbp
	ret

.section .text
.type render_text, @function
.globl render_text
/**
Renders text to the screen.

inputs:
	%r8d - Pointer to the TTF_Font.
	%rcx - Pointer to the SDL_Color for the text.
	%rdx - Pointer to the SDL_Rect where to render the text (i.e. location and size 
																  in the screen of 
																  the text).
	%rsi - Pointer to the text to render.
	%rdi - Pointer to SDL_Renderer.

Pseudo-code:
	void render_text(SDL_Renderer *renderer, char *text, SDL_Rect *rect, SDL_Color *color, TTF_Font *font) {
		SDL_Surface *surface = TTF_RenderText_Solid(font, &text, color);
        SDL_Texture *texture = SDL_CreateTextureFromSurface(renderer, surface);
        SDL_QueryTexture(texture, NULL, NULL, NULL, NULL);
        SDL_RenderCopy(renderer, texture, NULL, &text);
		// Free unnecessary objects
		SDL_FreeSurface(surface);
		SDL_DestroyTexture(texture);
	}
**/
render_text:
	pushq	%rbp
	movq	%rsp, %rbp
	subq	$56, %rsp

	movq	%r8, -40(%rbp)  # TTF_Font
	movq 	%rcx, -32(%rbp)  # SDL_Color
	movq	%rdx, -24(%rbp)  # SDL_Rect
	movq	%rsi, -16(%rbp)  # char*
	movq	%rdi, -8(%rbp)   # SDL_Renderer

    .render_solid_text:
        movq    -32(%rbp), %rdx # SDL_Color
        movq    -16(%rbp), %rsi
        movq    -40(%rbp), %rdi # TTF_Font pointer
        call    TTF_RenderText_Solid
        movq    %rax, -48(%rbp) # SDL_Surface
        cmpq    $0, -48(%rbp)
        jne     .create_texture
        movq    $0, %rax
        movl    $ERROR_TTF_RenderText_Solid, %edi
        call    SDL_Log
        call    release_memory
    .create_texture:
        movq    -48(%rbp), %rsi
        movq    -8(%rbp), %rdi
        call    SDL_CreateTextureFromSurface
        movq    %rax, -56(%rbp) # SDL_Texture
        cmpq    $0, -56(%rbp)
        jne     .sdl_query
        movq    $0, %rax
        movl    $ERROR_SDL_CreateTextureFromSurface, %edi
        call    SDL_Log
        movq    -48(%rbp), %rdi
        call    SDL_FreeSurface
        call    release_memory
    .sdl_query:
        movl    $0, %r8d
        movl    $0, %ecx
        movq    $0, %rdx
        movq    $0, %rsi
        movq    -56(%rbp), %rdi
        call    SDL_QueryTexture
    .draw_text_using_rect:
		movq    -24(%rbp), %rcx  # SDL_Rect
		movq    $0, %rdx
		movq    -56(%rbp), %rsi
		movq    -8(%rbp), %rdi
		call    SDL_RenderCopy
	.free_objects:
	    movq    -48(%rbp), %rdi
    	call    SDL_FreeSurface
    	movq    -56(%rbp), %rdi
    	call    SDL_DestroyTexture
	
	addq	$56, %rsp
	popq	%rbp
	ret
