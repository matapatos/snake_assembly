
ERROR_SDL_CreateTextureFromSurface:
    .asciz "Error using SDL_CreateTextureFromSurface\n"
ERROR_TTF_RenderText_Solid:
    .asciz "Error using TTF_RenderText_Solid\n"

.section .text
.type update_view, @function
.globl update_view
/**
Updates the UI.

NOTE: It does not require any input or output. It automatically uses the Game,
      Player and Seed properties.

Pseudo-code:
    void update_view() {
        global SDL_Renderer *renderer;
        global Player *me;
        global Seed *seed;
        global SDL_Color *score_color;
        global TTF_Font *score_font;
    
        SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
        SDL_RenderClear(renderer);

        # Render score points:
        SDL_Rect text_rect = {5,-2,60,60};
        if(me->points < 10)
            text_rect.w = 30;
        render_int(renderer, me->points, text_rect, score_color, score_font);

        # Render player and seed:
        SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255);
        SDL_RenderFillRects(renderer, me->body, me->num_parts);
        SDL_SetRenderDrawColor(renderer, 255, 0, 0, 255);
        SDL_RenderFillRect( renderer, &seed->body);
        SDL_RenderPresent(renderer);
    }
**/
update_view:
.prepare_view:
    movl    $255, %r8d
    movl    $0, %ecx
    movl    $0, %edx
    movl    $0, %esi
    movq    -88(%rbp), %rdi
    call    SDL_SetRenderDrawColor
    movq    -88(%rbp), %rdi
    call    SDL_RenderClear

.render_score:
    .init_font_rect:
        movl    $5, -2044(%rbp)
        movl    $-2, -2040(%rbp)
        movl    $60, -2036(%rbp)
        movl    $60, -2032(%rbp)
        cmpl    $9, -976(%rbp)
        jg      .draw_score
        movl    $30, -2036(%rbp)
    .draw_score:
        movq    -2056(%rbp), %r8  # TTF_Font
        leaq    -2048(%rbp), %rcx  # SDL_Color
        leaq    -2044(%rbp), %rdx  # SDL_Rect
        movl    -976(%rbp), %esi   # int score
        movq    -88(%rbp), %rdi    # SDL_Renderer
        call    render_int

.draw_player:
    movl    $255, %r8d
    movl    $255, %ecx
    movl    $255, %edx
    movl    $255, %esi 
    movq    -88(%rbp), %rdi
    call    SDL_SetRenderDrawColor
    # Player rendering code
    movl    -972(%rbp), %edx
    leaq    -968(%rbp), %rsi
    movq    -88(%rbp), %rdi
    call    SDL_RenderFillRects
.draw_seed:
    movl    $255, %r8d
    movl    $0, %ecx
    movl    $0, %edx
    movl    $255, %esi 
    movq    -88(%rbp), %rdi
    call    SDL_SetRenderDrawColor
    # Seed rendering code
    leaq    -996(%rbp), %rsi
    movq    -88(%rbp), %rdi
    call    SDL_RenderFillRect

.render_updated_view:
    movq    -88(%rbp), %rdi
    call    SDL_RenderPresent

    ret
