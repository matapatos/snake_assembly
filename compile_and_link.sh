# Delete existent files
rm game
rm game.o
# Compile audio C library
gcc -S external_c_libraries/audio.c
# Compile 
gcc -c game.s auxiliary.s update_view.s update_seed_location.s update_player_position.s check_collisions.s waiting_screen.s game_over.s audio.s
# Linking with SDL functions
gcc -gstabs game.o auxiliary.o update_view.o update_seed_location.o update_player_position.o check_collisions.o waiting_screen.o game_over.o audio.o `sdl2-config --libs --cflags` -ggdb3 -O0 --std=c99 -Wall -lSDL2_image -lSDL2_ttf -lm -o game