IMAGE_PATH:
    .asciz "./images/snake.png"
SDLK_SPACE:
    .int 32

.section .text
.type waiting_screen, @function
.globl waiting_screen
/**
Create initial Game screen.

NOTE: It does not require any input or output.

Pseudo-code:
    void waiting_screen() {
        global Player *me;
        global Game *game;

        render_waiting_image(IMAGE_FILEPATH);

        SDL_Event event;
        while(TRUE) {
            SDL_PollEvent(&event);
            if(event != NULL) {
                if(event.type == SDL_KEYDOWN) {
                    switch(event.key.keysym.sym) {
                        case SDLK_SPACE:
                            return;  // Return to body game execution code
                            break;
                        case SDLK_ESCAPE:
                            release_memory();
                            break;
                    }
                }
                else if(event.type == SDL_QUIT) {
                    release_memory();
                    break;
                }
            }
        }
    }
**/
waiting_screen:
    leaq    IMAGE_PATH, %rdi
    call    render_png_image

    .waiting_screen_loop:
        leaq    -1136(%rbp), %rdi
        call    SDL_PollEvent
        cmpl    $0, %eax
        je      .waiting_screen_loop  # Continue to Loop if no keyboard events
        .handle_io_events:
            .check_if_quit_event:
                movl    SDL_QUIT, %eax
                cmpl    -1136(%rbp), %eax
                jne     .check_key_down_events
                call    release_memory
            .check_key_down_events:
                movl    SDL_KEYDOWN, %eax
                cmpl    -1136(%rbp), %eax
                jne     .waiting_screen_loop
                .check_is_escape_key:
                    movl    SDLK_ESCAPE, %eax
                    cmpl    %eax, -1116(%rbp)
                    jne     .check_is_space_key
                    call    release_memory
                .check_is_space_key:
                    movl    SDLK_SPACE, %eax
                    cmpl    %eax, -1116(%rbp)
                    jne     .waiting_screen_loop
                    ret  # Return to game code
        jmp     .waiting_screen_loop
